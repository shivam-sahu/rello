import React from 'react';
import Header from './components/header/Header';
import Card from './components/card/card';

import './App.css';

function App() {
  return (
    <div className="App">
      <Header/>
      <Card/>
    </div>
  );
}

export default App;
