
let data = [
  {
    title: 'Task to do',
    todos: [
      {
        value: 'Cp'
      },
      {
        value: 'movie'
      }
    ]
  },
  {
    title: 'Doing',
    todos: [{
      value: 'React'
    }]
  }
];

const initialState = {
  editingTitleVal: '',
  editedValue: '',
  isEditing: false,
  isEditingTitle: false,
  isdone: true,
  lists: data,
  selectedValue: '',
  selectedIndex: '',
  selectedList: '',
}

const CardReducer = (state=initialState, action)=>{
  const {payload, type} = action;
  switch(type){

    case ('delete_task'):{
      const {event,listId, todoId} = payload;
      event.stopPropagation();

      const { todos: oldtodos = [], title } = state.lists[listId];
      const { lists: oldlist = [] } = state;
      const todos = [...oldtodos.slice(0, parseInt(todoId)), ...oldtodos.slice(parseInt(todoId) + 1)];
      const obj = { title, todos };
      const lists = [...oldlist.slice(0, parseInt(listId)), obj, ...oldlist.slice(parseInt(listId) + 1)];

      return{
        ...state,
        editedValue: '',
        lists,
        isEditing: false,
        selectedList: '',
        selectedIndex: ''
      }
    }

    case ('done_title_changes'):{
      const {event, listId} = payload;
      event.stopPropagation();

      const { todos = {} } = state.lists[listId];
      const { editingTitleVal = "Add title", lists: oldlists } = state;
      const obj = { title: editingTitleVal, todos };
      const lists = [...oldlists.slice(0, parseInt(listId)), obj, ...oldlists.slice(parseInt(listId) + 1)];

      return{
        ...state,
        lists,
        isEditingTitle: false,
        editingTitleVal: '',
        selectedList: ''
      }
      
    }
    case ('done_changes'):{
      const {event, listId, todoId} = payload;
      event.stopPropagation();
      const { todos: oldtodos = [], title } = state.lists[listId];
      const { editedValue, lists: oldlist = [] } = state;

      const newtodo = { value: editedValue };
      const todos = [...oldtodos.slice(0, parseInt(todoId)), newtodo, ...oldtodos.slice(parseInt(todoId) + 1)];
      const obj = { title, todos };
      const lists = [...oldlist.slice(0, parseInt(listId)), obj, ...oldlist.slice(parseInt(listId) + 1)];
      return{
        ...state,
        isEditing: false,
        editedValue: '',
        selectedList: '',
        selectedIndex: '',
        lists
      }
    }
    case ('edit_value_change'):{
        const editedValue=payload.value;
      return{
        ...state,editedValue
      }
    }
    case ('edit_title_change'):{
      const {value} = payload;
      return{
        ...state, editingTitleVal:value
      }
    }

    case ('handle_add_list'):{
      const { lists: oldLists } = state;
      const newList = {
        title: 'Add title',
        todos: []
      }
      const lists = [...oldLists, newList];
      return{
        ...state,lists
      };
    }

    case ('handle_add_task'):{
      payload.event.stopPropagation();
      const isdone =false;
      const selectedList=payload.listId
      return{
        ...state, isdone, selectedList
      }
    }
    case ('handle_edit_title'):{
      const {listId}=payload;
      const { title } = state.lists[listId];
      
      return{
        ...state,
        editingTitleVal: title,
        isEditingTitle: true,
        selectedList: listId
      }
    }
    case ('handle_done'):{
      const {e, listId} = payload;
      e.stopPropagation();
      const { todos: oldtodos = [], title } = state.lists[listId];
      const { selectedValue, lists: oldlist = [] } = state;
      const newtodo = { value: selectedValue };
      const todos = [...oldtodos, newtodo];
      const obj = { title, todos };
      const lists = [...oldlist.slice(0, parseInt(listId)), obj, ...oldlist.slice(parseInt(listId) + 1)];
      return{
        ...state, lists,
        isdone: true,
        selectedIndex: '',
        selectedValue: '' 
      }
    }

    case ('handle_move'):{
      const {event,listId, todoId, str} = payload;
      event.stopPropagation();

      const { editedValue, lists: oldlists = [] } = state;
      const { todos: oldtodos = [], title } = state.lists[listId];
      const todos = [...oldtodos.slice(0, parseInt(todoId)), ...oldtodos.slice(parseInt(todoId) + 1)];
      const objOne = { title, todos };

      const task = { value: editedValue };
      switch (str) {
        case 'nxt':
          if (listId < oldlists.length - 1) {
            const { todos: nextOldtodos, title: nextTitle } = state.lists[listId + 1];
            const secondTodos = [...nextOldtodos, task];
            const objTwo = { title: nextTitle, todos: secondTodos };
            const lists = [...oldlists.slice(0, parseInt(listId)), objOne, objTwo, ...oldlists.slice(parseInt(listId) + 2)]

            return{
              ...state,
              lists,
              editedValue: '',
              isEditing: false
            };
          }
          return null;
        case 'pre':
          if (listId > 0) {
            const { todos: nextOldtodos, title: nextTitle } = state.lists[listId - 1];
            const secondTodos = [...nextOldtodos, task];
            const objTwo = { title: nextTitle, todos: secondTodos };
            const lists = [...oldlists.slice(0, parseInt(listId) - 1), objTwo, objOne, ...oldlists.slice(parseInt(listId) + 1)]

            return{
              ...state,
              lists,
              editedValue: '',
              isEditing: false
            };
          }
          return null;
        default:
          return{
            ...state,
            editedValue: '',
            isEditing: false
          };
      }
    }

    case ('handle_task_click'):{
      const { event, listId, todoId} = payload;
      event.stopPropagation();
      const { value } = state.lists[listId].todos[todoId];
      return{
        ...state,
        isEditing: true,
        editedValue: value,
        selectedList: listId,
        selectedIndex: todoId
      }
    }
    case 'value_change':{
      const value= payload.value
      return{
        ...state,selectedValue:value
      }
    }
    default:
      return state;
  }
}

export default CardReducer;

