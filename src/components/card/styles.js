import { makeStyles } from '@material-ui/core/styles';

const styles =makeStyles(  themes=> ({

  root: {
    flexGrow: 1,
  },
  paper: {
    padding: themes.spacing(15),
    textAlign: 'center',
    color: themes.palette.text.secondary,
  },
  
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 24,
  },
  pos: {
    marginBottom: 12,
  },
  fab: {
      position: 'absolute',
      zIndex: 1,
      top: -50,
      left: 0,
      right: 0,
      margin: '0 auto',

  },
  extendedIcon: {
    marginRight: themes.spacing(1),
  },
  button: {
    margin: themes.spacing(1),
  },
  leftIcon: {
    marginRight: themes.spacing(1),
  },
  rightIcon: {
    marginLeft: themes.spacing(1),
  },
  iconSmall: {
    fontSize: 20,
  },
}));

export default styles;


