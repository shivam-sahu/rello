import React, { Component } from 'react';
import {connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import SaveIcon from '@material-ui/icons/Save';
import DeleteIcon from '@material-ui/icons/Delete';

//input fields
import clsx from 'clsx';
import TextField from '@material-ui/core/TextField';

//buttons
import Grid from '@material-ui/core/Grid';
import ButtonGroup from '@material-ui/core/ButtonGroup';

class Task extends  Component{
   
  render(){
    const {data, listId, todoId, lists} = this.props;
    const classes = this.props.classes;
    // const classes = this.props.classes;
    const bull = <span className={classes.bullet}>•</span>;
    // console.log("msg", this.props.cards.isEditing )
    return(<div>
      <div key={todoId} onClick={(e) => { this.handleTaskClick(e, listId, todoId) }}>
        {
          this.props.isEditing && listId === this.props.selectedList && todoId === this.props.selectedIndex ?
            <div>
              <TextField
                id="outlined-multiline-flexible"
                label="Todos"
                multiline
                rowsMax="4"
                value={this.props.editedValue}
                onChange={e => { this.editValueChange(e) }}
                className={classes.textField}
                margin="normal"
                variant="outlined"
              />
              <Button variant="contained"
                size="small"
                className={classes.button}
                onClick={(e) => { this.doneChanges(e, listId, todoId) }}>
                <SaveIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
                Save
                </Button>
              <Grid container>
                <Grid item>
                  <ButtonGroup
                    variant="contained"
                    color="primary"
                    aria-label="full-width contained primary button group"
                  >
                    <Button onClick={(e) => { this.deleteTask(e, listId, todoId) }}>
                      Delete
                    <DeleteIcon className={classes.rightIcon} />
                    </Button>
                    {listId < lists.length - 1 ?
                      <Button onClick={(e) => { this.handleMove(e, listId, todoId, "nxt") }}>MoveNext</Button>
                      :
                      null
                    }
                    {
                      listId > 0 ?
                        <Button onClick={(e) => { this.handleMove(e, listId, todoId, "pre") }}>MovePre</Button>
                        :
                        null
                    }
                  </ButtonGroup>
                </Grid>
              </Grid>
            </div>
            :
            <Typography variant="h5" component="h2">
              {bull} {data.value}
            </Typography>
        }
      </div>
    </div>
    );

  }
  handleTaskClick = (e, i, j) => {
    const payload = {
      event: e,
      listId: i,
      todoId: j
    }

    this.props.dispatch({ type: 'handle_task_click', payload });

  }

  editValueChange = (e) => {

    const value = e.target.value;
    this.props.dispatch({ type: 'edit_value_change', payload: { value } });

  }

  doneChanges = (e, i, j) => {
    const payload = {
      event: e,
      listId: i,
      todoId: j
    }

    this.props.dispatch({ type: 'done_changes', payload })
  }

  deleteTask = (e, i, j) => {
    const payload = {
      event: e,
      listId: i,
      todoId: j
    }
    this.props.dispatch({ type: 'delete_task', payload });
  }

  handleMove = (e, i, j, str) => {

    const payload = {
      event: e,
      listId: i,
      todoId: j,
      str
    }

    this.props.dispatch({ type: 'handle_move', payload });
  }


}

const mapStateToProps =(state)=>{
  console.log(state.cards.isEditing);
  const cards = state.cards;
  return cards;
}

const newTask = connect(mapStateToProps)(Task)
export default newTask;