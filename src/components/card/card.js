import React, { Component } from 'react';
import {connect } from 'react-redux';
import Task from './tasks/Task';
import Status from './status/status';

//material ui
import { withStyles } from '@material-ui/styles';
//card
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
//icons
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import SaveIcon from '@material-ui/icons/Save';
import DeleteIcon from '@material-ui/icons/Delete';

//input fields
import clsx from 'clsx';
import TextField from '@material-ui/core/TextField';

//buttons
import Grid from '@material-ui/core/Grid';

import styles from './styles'

class Template extends Component {

  render() {
    const { lists } = this.props;
    const classes = this.props.classes;
    const bull = <span className={classes.bullet}>•</span>;

    return (<div className="card">

      <div className={classes.root}>
        <Grid container spacing={2}>
        {lists ?
          lists.map((list, i) => (
            <Grid item xs={3}>
            <Card classes={classes.card}>
              <CardContent>
                  <div key={i} className='singleList'>
                    <Status listId={i} list={list} classes={classes} />
                    {
                      list.todos ?
                        list.todos.map((data, j) => (
                          <Task data={data} listId={i} todoId={j} classes={classes}
                          lists={lists}
                          />
                        )) : null
                    }
                    <div className='field'>
                      {
                        !this.props.isdone && this.props.selectedList === i ?
                        <div>
                        <TextField
                          id="outlined-multiline-flexible"
                          multiline
                          label="Todos"
                          rowsMax="4"
                          value={this.props.selectedValue}
                          onChange={(e) => { this.valueChange(e) }}
                          className={classes.textField}
                          margin="normal"
                          variant="outlined"
                          />
                            <Button variant="contained"
                              size="small"
                              className={classes.button}
                              onClick={(e) => { this.handleDone(e, i) }}>
                              <SaveIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
                              Save
                            </Button>
                          </div> 
                          : null
                      }
                    </div>
                    <div>
                      {
                        this.props.isdone ?
                        <CardActions>
                          <Button size="small" onClick={(e) => { this.handleAddTask(e, i) }}>Add Task</Button>
                        </CardActions>
                          : null
                      }
                    </div>
                  </div>
              </CardContent>
            </Card>
            </Grid>
          )) : null
        }
        </Grid>
      </div>

      <div className='addListBtn'>
        <Fab color="primary" aria-label="add" className={classes.fab} onClick={this.handleAddList}>
          <AddIcon />
        </Fab>
      </div>

    </div>)
  }

  handleAddTask = (e, i) => {
    const payload = {
      event:e,
      listId:i
    }
    this.props.dispatch({ type:'handle_add_task',payload })
  }

  handleAddList = () => {
    const payload ={};
    this.props.dispatch({type:'handle_add_list',payload})

  }

  handleDone = (e, i) => {
    const payload = {
      e,
      listId:i
    }
    this.props.dispatch({type:'handle_done', payload});

  }

  valueChange = (e) => {
    const payload = {value:e.target.value};
    this.props.dispatch({type:'value_change', payload});
  }
}

const mapStateToProps = (state)=>{

  const cards = state.cards;
  return cards;
}

const newTemplate = connect(mapStateToProps)(Template);

export default withStyles(styles)(newTemplate);