import React, {Component} from 'react';
import {connect } from 'react-redux';

import styles from '../styles';
import { withStyles } from '@material-ui/core';

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography'

//text field
import clsx from 'clsx';
import TextField from '@material-ui/core/TextField';

//icons
import SaveIcon from '@material-ui/icons/Save';

class Status extends Component{
  
  render(){
    const {classes, listId, list} = this.props;
    // console.log(this.props);
    return (<div>
      {
        this.props.isEditingTitle && listId === this.props.selectedList ?
          <div className="titleEdit">
            <TextField
              id="outlined-dense"
              label="Title"
              value={this.props.editingTitleVal}
              className={clsx(classes.textField, classes.dense)}
              onChange={e => { this.editTitleChange(e) }}
              margin="dense"
              variant="outlined"
            />
            <Button variant="contained"
              size="small"
              className={classes.button}
              onClick={(e) => { this.doneTitleChanges(e, listId) }}>
              <SaveIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
              Save
            </Button>
          </div>
          :
          <Typography className={classes.title}
            color="textSecondary" gutterBottom
            onClick={() => { this.handleEditTitle(listId) }}>
            {list.title}
          </Typography>
      }
    </div>);
  }
  
  editTitleChange = (e) => {
    const payload = {
      value: e.target.value
    }
    this.props.dispatch({ type: 'edit_title_change', payload });
  }

  doneTitleChanges = (e, i) => {
    const payload = {
      event: e,
      listId: i
    }
    this.props.dispatch({ type: 'done_title_changes', payload })

  }
  handleEditTitle = (i) => {
    
    const payload = {
      listId: i
    }
    this.props.dispatch({ type: 'handle_edit_title', payload });
  }
}

const mapStateToProps = (state, ownProps) => {

  const {cards:{editingTitleVal, lists,isEditingTitle,selectedList}={}} = state;

  return { editingTitleVal, lists, isEditingTitle, selectedList};
}

const newStatus = connect(mapStateToProps)(Status);

export default newStatus;