import React from 'react';
import FontAwesome from 'react-fontawesome';

import './header.css';

const Header =()=>{
  return(<div className='header'>
    <h1><span className='icon'><FontAwesome name='app_icon' className="fab fa-trello" /></span><span className='text'> Rello</span></h1>
  </div>);
}

export default Header;